#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

RED=1
ORANGE=2
GREEN=3

if [ $(free -b | grep 'Mem' | awk '{print $2}') -gt $((800 * 1024 ** 2)) ]; then
	echo 'This script must be run inside a VM with less than 800M of memory.'

	exit 1
fi

id0=$(docker run --rm -d -t --privileged -m 3G --memory-reservation 2500MB sysbench)
id1=$(docker run --rm -d -t --privileged -m 3G --memory-reservation 500MB sysbench)

# Get memory cgroup id.
cgroup_id0=$(cat /sys/fs/cgroup/memory/docker/$id0/memory.id)
cgroup_id1=$(cat /sys/fs/cgroup/memory/docker/$id1/memory.id)

# First run to prepare files
docker exec $id0 bash -c 'sysbench fileio --file-num=1 --file-total-size=3G prepare' > /dev/null
docker exec $id1 bash -c 'sysbench fileio --file-num=1 --file-total-size=3G prepare' > /dev/null

echo "Preparation finished!"

# Empty caches so first run does not impact the second.
echo 3 > /proc/sys/vm/drop_caches

# Clear the kernel log so if there were previous prints they will not make this
# script fail.
dmesg -C

# ALERT The message which will be "grepped" are printed by pr_debug.
# So the kernel MUST be compiled with CONFIG_DYNAMIC_DEBUG=y to enable them.
# With the following command we enable all the pr_debug() in file
# mm/memcontrol.c.
echo 'file mm/memcontrol.c +p' > /sys/kernel/debug/dynamic_debug/control

# Set cgroup0 as green and set cgroup1 as RED so cgroup0 will be reclaimed one
# time
echo $RED > /sys/fs/cgroup/memory/docker/$id1/memory.color
echo $GREEN > /sys/fs/cgroup/memory/docker/$id0/memory.color

# True run to create memory pressure.
docker exec $id0 bash -c 'sysbench --time=30 fileio --file-test-mode=seqrd --file-num=1 --file-total-size=3G run' > /tmp/$cgroup_id0 &
docker exec $id1 bash -c 'sysbench --time=30 fileio --file-test-mode=seqrd --file-num=1 --file-total-size=3G run' > /tmp/$cgroup_id1 &

sleep 5

# Set cgroup0 as green for the second time so its reclaimed flag will be cleared.
# So this cgroup will be reclaimed two times.
# We also set cgroup1 as orange so cgroup0 will be reclaimed.
echo $GREEN > /sys/fs/cgroup/memory/docker/$id0/memory.color
echo $ORANGE > /sys/fs/cgroup/memory/docker/$id1/memory.color

sleep 5

# Set cgroup0 as red so it will not be reclaimed anymore.
# We also set cgroup1 as orange so it will be reclaimed because cgroup0 is red.
echo $RED > /sys/fs/cgroup/memory/docker/$id0/memory.color
echo $ORANGE > /sys/fs/cgroup/memory/docker/$id1/memory.color

wait
echo "Both finished!"

# Clean everything.
docker exec $id0 bash -c 'sysbench fileio --file-num=1 --file-total-size=3G cleanup' > /tmp/$cgroup_id0 &
docker exec $id1 bash -c 'sysbench fileio --file-num=1 --file-total-size=3G cleanup' > /tmp/$cgroup_id1 &

docker stop $id0 $id1

time=$(dmesg | grep -P "Took enough memory" | wc -l)
if [ $time -ne 3 ]; then
	echo "There were not 3 special reclaims but $time!" 1>&2

	exit 1
fi

time=$(dmesg | grep -P "Took \d+ bytes from $cgroup_id0" | wc -l)
if [ $time -ne 2 ]; then
	echo "$cgroup_id0 was not reclaimed twice but $time!" 1>&2

	exit 1
fi

time=$(dmesg | grep -P "Took \d+ bytes from $cgroup_id1" | wc -l)
if [ $time -ne 1 ]; then
	echo "$cgroup_id1 was not reclaimed once but $time!" 1>&2

	exit 1
fi