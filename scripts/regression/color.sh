#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

id=$(docker run --rm -d -t --privileged debian)

# Get the value from outside and inside the container.
cat_from_host=$(cat /sys/fs/cgroup/memory/docker/$id/memory.color)
cat_from_container=$(docker exec $id bash -c 'cat /sys/fs/cgroup/memory/memory.color')

# If the values do not match there is a big problem.
if [ $cat_from_container != $cat_from_host ]; then
	echo "Problem was met while reading color : $cat_from_container != $cat_from_host" 1>&2

	exit
fi

# Update the value and get it from inside the container.
docker exec $id bash -c 'echo 1 > /sys/fs/cgroup/memory/memory.color'
cat_from_container=$(docker exec $id bash -c 'cat /sys/fs/cgroup/memory/memory.color')

# The values should not match.
if [ $cat_from_container == $cat_from_host ] ;then
	echo "Problem was met while reading color : $cat_from_container == $cat_from_host" 1>&2

	exit
fi

# Get the value from outside the container.
cat_from_host=$(cat /sys/fs/cgroup/memory/docker/$id/memory.color)

# Now the values should match.
if [ $cat_from_container != $cat_from_host ]; then
	echo "Problem was met while reading color : $cat_from_container != $cat_from_host" 1>&2

	exit
fi

docker stop $id