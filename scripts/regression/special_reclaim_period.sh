#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

id0=$(docker run --rm -d -t --privileged debian)
id1=$(docker run --rm -d -t --privileged debian)

# Get the value from outside and inside the container.
cat_from_host=$(cat /sys/fs/cgroup/memory/docker/$id0/memory.special_reclaim_period)
cat_from_container=$(docker exec $id0 bash -c 'cat /sys/fs/cgroup/memory/memory.special_reclaim_period')

# If the values do not match there is a big problem.
if [ $cat_from_container != $cat_from_host ]; then
	echo "Problem was met while reading special_reclaim_period : $cat_from_container != $cat_from_host" 1>&2

	exit
fi

# Update the value and get it from inside the container.
docker exec $id0 bash -c 'echo 10 > /sys/fs/cgroup/memory/memory.special_reclaim_period'
cat_from_container=$(docker exec $id0 bash -c 'cat /sys/fs/cgroup/memory/memory.special_reclaim_period')

# The values should not match.
if [ $cat_from_container == $cat_from_host ] ;then
	echo "Problem was met while reading special_reclaim_period : $cat_from_container == $cat_from_host" 1>&2

	exit
fi

# Get the value from oustide the container.
cat_from_host=$(cat /sys/fs/cgroup/memory/docker/$id0/memory.special_reclaim_period)

# Now the values should match.
if [ $cat_from_container != $cat_from_host ]; then
	echo "Problem was met while reading special_reclaim_period : $cat_from_container != $cat_from_host" 1>&2

	exit
fi

# Get the value from the second container
cat_from_container=$(docker exec $id1 bash -c 'cat /sys/fs/cgroup/memory/memory.special_reclaim_period')

# This value is global.
# So getting it from any container should return the same value.
if [ $cat_from_container != $cat_from_host ]; then
	echo "Problem was met while reading special_reclaim_period : $cat_from_container != $cat_from_host" 1>&2

	exit
fi

# Reset the value to its default value.
docker exec $id0 bash -c 'echo 1 > /sys/fs/cgroup/memory/memory.special_reclaim_period'

docker stop $id0
docker stop $id1