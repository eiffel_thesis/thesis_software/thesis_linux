# Linux kernel for MemOpLight

Modifications to Linux kernel 4.19 to guide memory reclaim for containers based on their performance.

## Getting started

To compile the kernel use the following commands:

```bash
# Get a minimal config for x86 64 bits architectures.
make x86_64_defconfig

# Add configurations for virtualization.
make kvmconfig

# Add minimal configurations to run docker.
make docker.config

# Add debug options.
make debug.config
```

You should have obtain a bootable image of this modified Linux kernel in `arch/x86/boot/bzImage`.
You can now use this image as kernel for a VM run with `qemu`.

## Modifications

The modifications brought the kernel are mainly in `mm/memcontrol.c` and `include/linux/memcontrol.h`.
They target the memory cgroup and especially the `soft limit` reclaim.

### `Soft limit` background

Basically, the `soft limit` is limit what can be given for each memory cgroup.
When memory pressure occurs, the kernel will try to satisfy this amount of memory for the cgroup.
The goal of this limit is to be set to the working set of the containerized application.
But this limit comes without any warranty.

### Performance-guided reclaim

I modified the kernel to guide reclaim with containers performance.
Essentially, the containers with good performance will be reclaimed, so the reclaimed memory will be free and container with bad performance can take it in the hope to increase their performance.
The reclaim takes percent of memory to good performing containers periodically.

### Userland interactions

Multiples files were added in the `sysfs` so userland can interact with the modified reclaim:

- `/sys/fs/cgroup/memory/X/memory.color`: It indicates the container performance.
There are four possible values:

	- 0 means the container has no color, while this value remains it is out of the modified reclaim. This is the default value.
	- 1 means the container has bad performance.
	- 2 means the container has satisfying performance, it respects its SLO, but can do better.
	- 3 means the container has the best possible performance.

- `/sys/fs/cgroup/memory/X/memory.percent_to_take`: It indicates the percent of memory that will be reclaim for this cgroup.
Default value is 2%.
- `/sys/fs/cgroup/memory/X/memory.special_reclaim_period` : It indicates the reclaim period of the mechanism.
It is given in second.
Note that, compared to above files, this file is global, _i.e._ setting this value for one cgroup will set for all cgroup since the reclaim is global.

### Code modifications

As stated above, the modifications target mainly in `mm/memcontrol.c` and `include/linux/memcontrol.h`.
In `mem_cgroup_soft_limit_reclaim`, new code was added to call the new function `mem_cgroup_soft_limit_special_reclaim` each period.
`mem_cgroup_soft_limit_special_reclaim` is the entry point of the performance-guided reclaim.
Note that, if this reclaim failed to reclaim enough memory, it falls through the `soft limit` but if it succeeded, the `soft limit` and the reclaim will not be called until next period.

## Authors

**Francis Laniel** [<francis.laniel@lip6.fr>](francis.laniel@lip6.fr)

This project is based on the Linux kernel 4.19 so I am only the author of the modifications I brought to this kernel not of the whole thing.

## License

This project is licensed under the GPLv2 License.